#ifndef PID_H
#define PID_H

#include <vector>

using namespace std;

class PID 
{
public:
  /* Twiddle related */
	vector<double> p;
	vector<double> dp;
	double tol;
	int nsteps;
	double best_err;
	double cum_tot_error;

  /*
    Previous value of cte
  */
  double prev_cte;
  /*
  * Errors
  */
  double p_error;
  double i_error;
  double d_error;

  /*
  * Coefficients
  */ 
  double Kp;
  double Ki;
  double Kd;

  /*
  * Constructor
  */
  PID();

  /*
  * Destructor.
  */
  virtual ~PID();

  /*
  * Initialize PID.
  */
  void Init(double Kp, double Ki, double Kd);

  /*
  * Update the PID error variables given cross track error.
  */
  void UpdateError(double cte);

  /*
  * Calculate the total PID error.
  */
  double TotalError();

  /*
  Twiddle algorithm to tune the parameters  
  */
  void Twiddle();

  double CalcError();  

  double DpSum();
};

#endif /* PID_H */
