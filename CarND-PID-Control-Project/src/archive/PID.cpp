#include <iostream>
#include "PID.h"

using namespace std;

const int num_params = 3;

/*
* TODO: Complete the PID class.
*/

PID::PID() {}

PID::~PID() {}

void PID::Init(double Kp_in, double Ki_in, double Kd_in) 
{
	Kp = Kp_in;
	Ki = Ki_in;
	Kd = Kd_in;

	prev_cte = 0.76;
	
	p_error = 0;
	d_error = 0;
	i_error = 0;

	p.resize(num_params);
	p[0] = Kp_in;
	p[1] = Ki_in;
	p[2] = Kd_in;	
	dp.resize(num_params);
	dp[0] = 1.0;
	dp[1] = 1.0;
	dp[2] = 1.0;
	tol = 0.000001;
	cum_tot_error = 0.0;
	nsteps = 0;
	best_err = 9999;
}

void PID::UpdateError(double cte) 
{
	nsteps += 1;

	p_error = cte;
	d_error = cte - prev_cte;
	i_error += cte;
	prev_cte = cte;

	return;
}

double PID::TotalError() 
{
	double total_error = p[0] * p_error + p[1] * i_error + p[2] * d_error;

	return total_error; 
}

double PID::CalcError()
{
	double total_error = p[0] * p_error + p[1] * i_error + p[2] * d_error;

	cum_tot_error += (total_error*total_error);

	return (cum_tot_error / nsteps);
}

double PID::DpSum()
{
	double dp_sum = 0;
	for (int i = 0; i < num_params; ++i)
	{
		dp_sum += dp[i];
	}

	return dp_sum;
}

void PID::Twiddle()
{
	double error = 0.0;

	while (DpSum() > tol)
	{
		for (int i = 0; i < num_params; ++i)		
		{		
			p[i] += dp[i];
			error = CalcError();

			//if (i == 2) std::cout << "0. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;

			if (error < best_err)
			{
				//if (i == 2) std::cout << "1. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;

				best_err = error;
				dp[i] *= 1.1;
			}
			else
			{
				p[i] -= 2 * dp[i];
				error = CalcError();
				//if (i == 2) std::cout << "2. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;

				if (error < best_err)
				{
					//if (i == 2) std::cout << "3. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;
					
					best_err = error;
					dp[i] *= 1.1;
				}
				else
				{	
					p[i] += dp[i];
					dp[i] *= 0.9;

					//if (i == 2) std::cout << "4. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;
				}
			}
		}		
	}	

	return;
}


