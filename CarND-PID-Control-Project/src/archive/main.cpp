#include <uWS/uWS.h>
#include <iostream>
#include "json.hpp"
#include "PID.h"
#include <math.h>
#include <vector>
#include <fstream>

using namespace std;

// for convenience
using json = nlohmann::json;

// For converting back and forth between radians and degrees.
constexpr double pi() { return M_PI; }
double deg2rad(double x) { return x * pi() / 180; }
double rad2deg(double x) { return x * 180 / pi(); }

// Checks if the SocketIO event has JSON data.
// If there is data the JSON object in string format will be returned,
// else the empty string "" will be returned.
std::string hasData(std::string s) 
{
  auto found_null = s.find("null");
  auto b1 = s.find_first_of("[");
  auto b2 = s.find_last_of("]");
  if (found_null != std::string::npos) 
  {
    return "";
  }
  else if (b1 != std::string::npos && b2 != std::string::npos) 
  {
    return s.substr(b1, b2 - b1 + 1);
  }
  return "";
}

int num_iterations = 0;
bool reset = true;

double best_error_t = 9999;
double best_error_a = 9999;
vector<double> best_p_a(3);
vector<double> best_p_t(3);

//ofstream resultsFile;

int main()
{
  //resultsFile.open("pid_results.txt", ios::app | ios::ate);
  //resultsFile << "cte" << "," << "speed" << "," << "angle" << "," << "steer_value" << "," << "throttle_value" << "\n";
  
  uWS::Hub h;

  PID pid_a, pid_t;
  double Kp_a = 0.1;
  double Ki_a = 0.0001;
  double Kd_a = 3.0;
  std::cout << "Kp_a: " << Kp_a << " Ki_a: " << Ki_a << " Kd_a: " << Kd_a << std::endl;  

  // Initialize the pid variable.
  pid_a.Init(Kp_a, Ki_a, Kd_a);
  best_p_a = { Kp_a, Ki_a, Kd_a };

  double Kp_t = 0.0; // 0.3;
  double Ki_t = 0.0;
  double Kd_t = 0.0; // 0.02;
  pid_t.Init(Kp_t, Ki_t, Kd_t);
  best_p_t = { Kp_t, Ki_t, Kd_t };

  std::cout << "Kp_t: " << Kp_t << " Ki_t: " << Ki_t << " Kd_t: " << Kd_t << std::endl;

  h.onMessage([&pid_a, &pid_t](uWS::WebSocket<uWS::SERVER>* ws, char *data, size_t length, uWS::OpCode opCode)
  {
    // "42" at the start of the message means there's a websocket message event.
    // The 4 signifies a websocket message
    // The 2 signifies a websocket event
    if (length && length > 2 && data[0] == '4' && data[1] == '2')
    {
      auto s = hasData(std::string(data).substr(0, length));
      if (s != "") 
	  {
        auto j = json::parse(s);
        std::string event = j[0].get<std::string>();
		if (event == "telemetry")
		{
			// j[1] is the data JSON object
			double cte = std::stod(j[1]["cte"].get<std::string>());
			double speed = std::stod(j[1]["speed"].get<std::string>());
			double angle = std::stod(j[1]["steering_angle"].get<std::string>());

			pid_a.UpdateError(cte);
			double steer_value = -1.0*pid_a.TotalError();

			pid_t.UpdateError(fabs(cte));
			double throttle_value = 0.7 - pid_t.TotalError();

			if (1)
			{	
				double steps_thresh = 2500;
				if (num_iterations < 100)
				{
					steps_thresh = 500;
				}
				if (pid_t.nsteps > steps_thresh)
				{
					num_iterations += 1;
					pid_t.Twiddle();
					std::cout << "Iteration: " << num_iterations << std::endl;
					std::cout << "Kp_t: " << pid_t.p[0] << " Ki_t: " << pid_t.p[1] << " Kd_t: " << pid_t.p[2] << std::endl;
					std::cout << "Error_t: " << pid_t.CalcError() << std::endl;		

					pid_t.nsteps = 0;
					pid_t.cum_tot_error = 0;

					/*
					pid_a.Twiddle();
					std::cout << "Kp_a: " << pid_a.p[0] << " Ki_a: " << pid_a.p[1] << " Kd_a: " << pid_a.p[2] << std::endl;
					std::cout << "Error_a: " << pid_a.CalcError() << std::endl;
					*/

					// reset simulator
					if (reset)
					{
						std::string reset_msg = "42[\"reset\",{}]";
						ws->send(reset_msg.data(), reset_msg.length(), uWS::OpCode::TEXT);
					
						/*
						if (pid_a.CalcError() < best_error_a)
						{
							best_error_a = pid_a.CalcError();
							best_p_a = { pid_a.p[0], pid_a.p[1], pid_a.p[2] };
						}

						pid_a.Init(best_p_a[0], best_p_a[1], best_p_a[2]);
						pid_a.best_err = best_error_a;
						*/

						if (pid_t.CalcError() < best_error_t)
						{
							best_error_t = pid_t.CalcError();
							best_p_t = { pid_t.p[0], pid_t.p[1], pid_t.p[2] };
						}

						pid_t.Init(best_p_t[0], best_p_t[1], best_p_t[2]);
						pid_t.best_err = best_error_t;						
					}
				}
			}
		 
          // DEBUG
		  //std::cout << "CTE: " << cte << " Steering Value: " << steer_value << "Throttle Value: " << throttle_value << std::endl;

          json msgJson;
          msgJson["steering_angle"] = steer_value;
		  msgJson["throttle"] = throttle_value;  
          auto msg = "42[\"steer\"," + msgJson.dump() + "]";
          // std::cout << msg << std::endl;
          ws->send(msg.data(), msg.length(), uWS::OpCode::TEXT);

		  //resultsFile << cte << "," << speed << "," << angle << "," << steer_value << "," << throttle_value << "\n";

		  //if (pid_a.nsteps == 3000) resultsFile.close();
        }
      } 
	  else 
	  {
        // Manual driving
        std::string msg = "42[\"manual\",{}]";
        ws->send(msg.data(), msg.length(), uWS::OpCode::TEXT);
      }
    }
  });

  // We don't need this since we're not using HTTP but if it's removed the program
  // doesn't compile :-(
  h.onHttpRequest([](uWS::HttpResponse *res, uWS::HttpRequest req, char *data, size_t, size_t) 
  {
    const std::string s = "<h1>Hello world!</h1>";
    if (req.getUrl().valueLength == 1)
    {
      res->end(s.data(), s.length());
    }
    else
    {
      // i guess this should be done more gracefully?
      res->end(nullptr, 0);
    }
  });

  h.onConnection([&h](uWS::WebSocket<uWS::SERVER>* ws, uWS::HttpRequest req) 
  {
    std::cout << "Connected!!!" << std::endl;
  });

  h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER>* ws, int code, char *message, size_t length) 
  {
    ws->close();
    std::cout << "Disconnected" << std::endl;
  });

  int port = 4567;
  if (h.listen("127.0.0.1", port))
  {
    std::cout << "Listening to port " << port << std::endl;
  }
  else
  {
    std::cerr << "Failed to listen to port" << std::endl;
    return -1;
  }
  h.run();
}
