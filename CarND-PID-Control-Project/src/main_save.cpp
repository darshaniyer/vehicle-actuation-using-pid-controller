#include <uWS/uWS.h>
#include <iostream>
#include "json.hpp"
#include "PID.h"
#include <math.h>
#include <vector>
#include <fstream>

using namespace std;

// for convenience
using json = nlohmann::json;

// For converting back and forth between radians and degrees.
constexpr double pi() { return M_PI; }
double deg2rad(double x) { return x * pi() / 180; }
double rad2deg(double x) { return x * 180 / pi(); }

// Checks if the SocketIO event has JSON data.
// If there is data the JSON object in string format will be returned,
// else the empty string "" will be returned.
string hasData(string s) 
{
  auto found_null = s.find("null");
  auto b1 = s.find_first_of("[");
  auto b2 = s.find_last_of("]");
  if (found_null != string::npos) 
  {
    return "";
  }
  else if (b1 != string::npos && b2 != string::npos) 
  {
    return s.substr(b1, b2 - b1 + 1);
  }
  return "";
}

void reset_simulator(uWS::WebSocket<uWS::SERVER>* ws, uWS::OpCode opCode)
{
	string reset_msg = "42[\"reset\",{}]";
	ws->send(reset_msg.data(), reset_msg.length(), uWS::OpCode::TEXT);
}

double run_simulator(vector<double>& p_a, vector<double>& p_t, PID& pid_a, PID& pid_t, 
	                 uWS::WebSocket<uWS::SERVER>* ws, char* data, size_t length, uWS::OpCode opCode)
{
	//reset_simulator(ws, opCode);
	pid_a.Init(p_a[0], p_a[1], p_a[2]);
	pid_t.Init(p_t[0], p_t[1], p_t[2]);

	int nsteps = 0;
	double cumulative_error = 0;
	while (nsteps < 500)
	{
		// "42" at the start of the message means there's a websocket message event.
		// The 4 signifies a websocket message
		// The 2 signifies a websocket event
		if (length && length > 2 && data[0] == '4' && data[1] == '2')
		{
			auto s = hasData(string(data).substr(0, length));
			if (s != "")
			{
				auto j = json::parse(s);
				string event = j[0].get<string>();
				if (event == "telemetry")
				{		
					// j[1] is the data JSON object
					double cte = stod(j[1]["cte"].get<string>());
					double speed = stod(j[1]["speed"].get<string>());
					double angle = stod(j[1]["steering_angle"].get<string>());

					pid_a.UpdateError(cte);
					double steer_value = -1.0*pid_a.TotalError();

					pid_t.UpdateError(fabs(cte));
					double throttle_value = 0.7 - pid_t.TotalError();

					cumulative_error += pid_t.TotalError()*pid_t.TotalError();

					json msgJson;
					msgJson["steering_angle"] = steer_value;
					msgJson["throttle"] = throttle_value;
					auto msg = "42[\"steer\"," + msgJson.dump() + "]";
					// cout << msg << endl;
					ws->send(msg.data(), msg.length(), uWS::OpCode::TEXT);
				}
			}
			else
			{
				// Manual driving
				string msg = "42[\"manual\",{}]";
				ws->send(msg.data(), msg.length(), uWS::OpCode::TEXT);
			}
		}
		nsteps++;
	}

	cout << (cumulative_error / nsteps) << endl;

	return  (cumulative_error / nsteps);	
}

double DpSum(vector<double>& dp, int num_params)
{
	double dp_sum = 0;
	for (int i = 0; i < num_params; ++i) dp_sum += dp[i];

	return dp_sum;
}

double Twiddle(vector<double>& p, vector<double>& dp, vector<double>& p_a, PID& pid_a, PID& pid_t, 
			   uWS::WebSocket<uWS::SERVER>* ws, char *data, size_t length, uWS::OpCode opCode)
{
	dp = { 1, 1, 1 };
	double error;
	double tol = 0.00001;
	int num_params = 3;
	double best_error = run_simulator(p_a, p, pid_a, pid_t, ws, data, length, opCode);

	while (DpSum(dp, num_params) > tol)
	{
		for (int i = 0; i < num_params; ++i)
		{
			p[i] += dp[i];
			error = run_simulator(p_a, p, pid_a, pid_t, ws, data, length, opCode);

			//if (i == 2) cout << "0. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;

			if (error < best_error)
			{
				//if (i == 2) cout << "1. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;

				best_error = error;
				dp[i] *= 1.1;
			}
			else
			{
				p[i] -= 2 * dp[i];
				error = run_simulator(p_a, p, pid_a, pid_t, ws, data, length, opCode);
				//if (i == 2) cout << "2. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;

				if (error < best_error)
				{
					//if (i == 2) cout << "3. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;

					best_error = error;
					dp[i] *= 1.1;
				}
				else
				{
					p[i] += dp[i];
					dp[i] *= 0.9;

					//if (i == 2) cout << "4. I was here" << " Kd = " << p[i] << "  dp[2] = " << dp[2] << endl;
				}
			}
		}	
	}

	return best_error;
}

int num_iterations = 0;

vector<double> p_a(3);
vector<double> p_t(3);

vector<double> dp(3);

//ofstream resultsFile;

int main()
{
  //resultsFile.open("pid_results.txt", ios::app | ios::ate);
  //resultsFile << "cte" << "," << "speed" << "," << "angle" << "," << "steer_value" << "," << "throttle_value" << "\n";
  
  uWS::Hub h;

  PID pid_a, pid_t;
  double Kp_a = 0.1;
  double Ki_a = 0.0001;
  double Kd_a = 3.0;
  cout << "Kp_a: " << Kp_a << " Ki_a: " << Ki_a << " Kd_a: " << Kd_a << endl;  
  p_a = { Kp_a, Ki_a, Kd_a };

  // Initialize the pid variable.
  pid_a.Init(Kp_a, Ki_a, Kd_a);
  //best_p_a = { Kp_a, Ki_a, Kd_a };

  double Kp_t = 0.3; // 0.3;
  double Ki_t = 0.0;
  double Kd_t = 0.02; // 0.02;
  pid_t.Init(Kp_t, Ki_t, Kd_t);
  p_t = { Kp_t, Ki_t, Kd_t };
  //best_p_t = { Kp_t, Ki_t, Kd_t };

  cout << "Kp_t: " << Kp_t << " Ki_t: " << Ki_t << " Kd_t: " << Kd_t << endl;

  h.onMessage([&pid_a, &pid_t](uWS::WebSocket<uWS::SERVER>* ws, char *data, size_t length, uWS::OpCode opCode)
  {   	
	while (num_iterations < 3)
	{
		double twiddle_error = Twiddle(p_t, dp, p_a, pid_a, pid_t, ws, data, length, opCode);

		cout << "Kp_t: " << p_t[0] << " Ki_a: " << p_t[1] << " Kd_a: " << p_t[2] << " Error: " << twiddle_error << endl;

		num_iterations++;
	}

    // DEBUG
	//cout << "CTE: " << cte << " Steering Value: " << steer_value << "Throttle Value: " << throttle_value << endl;



	//resultsFile << cte << "," << speed << "," << angle << "," << steer_value << "," << throttle_value << "\n";

	//if (pid_a.nsteps == 3000) resultsFile.close();

  });

  // We don't need this since we're not using HTTP but if it's removed the program
  // doesn't compile :-(
  h.onHttpRequest([](uWS::HttpResponse *res, uWS::HttpRequest req, char *data, size_t, size_t) 
  {
    const string s = "<h1>Hello world!</h1>";
    if (req.getUrl().valueLength == 1)
    {
      res->end(s.data(), s.length());
    }
    else
    {
      // i guess this should be done more gracefully?
      res->end(nullptr, 0);
    }
  });

  h.onConnection([&h](uWS::WebSocket<uWS::SERVER>* ws, uWS::HttpRequest req) 
  {
    cout << "Connected!!!" << endl;
  });

  h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER>* ws, int code, char *message, size_t length) 
  {
    ws->close();
    cout << "Disconnected" << endl;
  });

  int port = 4567;
  if (h.listen("127.0.0.1", port))
  {
    cout << "Listening to port " << port << endl;
  }
  else
  {
    cerr << "Failed to listen to port" << endl;
    return -1;
  }
  h.run();
}

