#include <uWS/uWS.h>
#include <iostream>
#include "json.hpp"
#include "PID.h"
#include <math.h>
#include <vector>
#include <fstream>

using namespace std;

// for convenience
using json = nlohmann::json;

// For converting back and forth between radians and degrees.
constexpr double pi() { return M_PI; }
double deg2rad(double x) { return x * pi() / 180; }
double rad2deg(double x) { return x * 180 / pi(); }

// Checks if the SocketIO event has JSON data.
// If there is data the JSON object in string format will be returned,
// else the empty string "" will be returned.
string hasData(string s) 
{
  auto found_null = s.find("null");
  auto b1 = s.find_first_of("[");
  auto b2 = s.find_last_of("]");
  if (found_null != string::npos) 
  {
    return "";
  }
  else if (b1 != string::npos && b2 != string::npos) 
  {
    return s.substr(b1, b2 - b1 + 1);
  }
  return "";
}

// resets the simulator and places the car at the starting position
void reset_simulator(uWS::WebSocket<uWS::SERVER>* ws, uWS::OpCode opCode)
{
	string reset_msg = "42[\"reset\",{}]";
	ws->send(reset_msg.data(), reset_msg.length(), uWS::OpCode::TEXT);
}

// runs one step of the simulator 
void run_simulator_internal(const vector<double>& p_a, const vector<double>& p_t, PID& pid_a, PID& pid_t,
				            uWS::WebSocket<uWS::SERVER>* ws, char* data, size_t length, uWS::OpCode opCode, 
	                        ofstream& resultsFile)
{
	// "42" at the start of the message means there's a websocket message event.
	// The 4 signifies a websocket message
	// The 2 signifies a websocket event

	if (length && length > 2 && data[0] == '4' && data[1] == '2')
	{
		auto s = hasData(string(data).substr(0, length));
		if (s != "")
		{
			auto j = json::parse(s);
			string event = j[0].get<string>();
			if (event == "telemetry")
			{
				// j[1] is the data JSON object
				double cte = stod(j[1]["cte"].get<string>());
				double speed = stod(j[1]["speed"].get<string>());
				double angle = stod(j[1]["steering_angle"].get<string>());

				pid_a.update_error(cte);
				double steer_value = -1.0*pid_a.total_error();

				pid_t.update_error(fabs(cte));
				double throttle_value = 0.6 - pid_t.total_error();

				// DEBUG
				// cout << "Step: " << pid_t.nsteps << " CTE: " << cte << "  Steering Value: " << steer_value << "  Throttle Value: " << throttle_value << endl;

				json msgJson;
				msgJson["steering_angle"] = steer_value;
				msgJson["throttle"] = throttle_value;
				auto msg = "42[\"steer\"," + msgJson.dump() + "]";
				// cout << msg << endl;
				ws->send(msg.data(), msg.length(), uWS::OpCode::TEXT);

				resultsFile << cte << "," << steer_value << "," << throttle_value << "\n";

				if (pid_a.nsteps == 3000) resultsFile.close();
			}
		}
		else
		{
			// Manual driving
			string msg = "42[\"manual\",{}]";
			ws->send(msg.data(), msg.length(), uWS::OpCode::TEXT);
		}
	}

	return;
}

// calls reset_simulator() and initializes the PID controllers with the updated values of the parameters
void reset_and_init(const vector<double>& p_a, const vector<double>& p_t, PID& pid_a, PID& pid_t, 
					uWS::WebSocket<uWS::SERVER>* ws, uWS::OpCode opCode)
{
	reset_simulator(ws, opCode);

	pid_a.init(p_a[0], p_a[1], p_a[2]);
	pid_t.init(p_t[0], p_t[1], p_t[2]);

	return;
}

// calculates the sum of the probing values for all the PID parameters for twiddle algorithm
double dp_sum(vector<double>& dp, int num_params)
{
	double dp_tot = 0;
	for (int i = 0; i < num_params; ++i) dp_tot += dp[i];

	return dp_tot;
}


// calls the simulator and passes in the PID parameters for steering angle and throttle to the simulator
// runs twiddle if twiddle = true
const int num_params = 3;
const double tol = 0.00001;
const int nsteps_settle = 100;
int nsteps_lim = 2800;
double best_error = 9999;
int index = 2;
int sc_index = 0;
int it = 0;
bool twiddle = false;
vector<double> best_p(3);
vector<double> error_progress(1000);
void run_simulator(uWS::Hub& h, vector<double>& p_a, vector<double>& p_t, PID& pid_a, PID& pid_t, vector<double>& dp, double& error, ofstream& resultsFile)
{
	h.onMessage([&pid_a, &pid_t, &p_a, &p_t, &dp, &error, &resultsFile](uWS::WebSocket<uWS::SERVER>* ws, char *data, size_t length, uWS::OpCode opCode)
	{
		run_simulator_internal(p_a, p_t, pid_a, pid_t, ws, data, length, opCode, resultsFile);

		if (twiddle)
		{
			if (dp_sum(dp, num_params) > tol)
			{
				if (pid_a.nsteps == nsteps_settle) // allow the simulator to settle for 100 steps
				{
					pid_a.cum_tot_error = 0;
				}
				else if (pid_a.nsteps >= nsteps_lim + nsteps_settle)
				{
					error = pid_a.cum_tot_error / nsteps_lim;
					error_progress[it] = error;

					if (sc_index == 0)
					{
						p_a[index] += dp[index];
						sc_index++;
					}
					else if (sc_index == 1)
					{
						if (error < best_error)
						{
							best_error = error;
							dp[index] *= 1.1;

							sc_index = 0;
							index = (index + 1) % 3;
							// if (index == 1) index += 1; // ignore i term for throttle
							best_p = { p_a[0], p_a[1], p_a[2] };
						}
						else
						{
							p_a[index] -= 2 * dp[index];
							sc_index++;
						}
					}
					else if (sc_index == 2)
					{
						if (error < best_error)
						{
							best_error = error;
							dp[index] *= 1.1;
							best_p = { p_a[0], p_a[1], p_a[2] };
						}
						else
						{
							p_a[index] += dp[index];
							dp[index] *= 0.9;
						}
						sc_index = 0;
						index = (index + 1) % 3;
						// if (index == 1) index += 1; // ignore i term for throttle
					}

					it++;
					cout << " Iteration: " << it << endl;
					cout << " Kp: " << p_a[0] << " Ki: " << p_a[1] << " Kd: " << p_a[2] << endl;
					cout << " dp1: " << dp[0] << " dp2: " << dp[1] << " dp3: " << dp[2] << endl;
					cout << " Best error: " << best_error << "  Current error: " << error << endl;

					reset_and_init(p_a, p_t, pid_a, pid_t, ws, opCode);
				}
			}
			else // if (dp_sum(dp, num_params) < tol)
			{
				cout << "Converged iteration: " << it << endl;
				cout << "Best error: " << best_error << endl;
				cout << " Kp: " << best_p[0] << " Ki: " << best_p[1] << " Kd: " << best_p[2] << endl;
				cout << "Last error: " << error << endl;
				cout << " Kp: " << p_a[0] << " Ki: " << p_a[1] << " Kd: " << p_a[2] << endl;
				cout << " dp1: " << dp[0] << " dp2: " << dp[1] << " dp3: " << dp[2] << endl;
			}
		}
	});
}

bool is_connected = false;

vector<double> p_a(3);
vector<double> p_t(3);
vector<double> dp(3);

ofstream resultsFile;

int main()
{
	resultsFile.open("pid_results.txt", ios::app | ios::ate);
	resultsFile << "cte" << "," << "steer_value" << "," << "throttle_value" << "\n";
	bool is_connected = false;
	uWS::Hub h;

	PID pid_a, pid_t;

	// final set of PID parameters for controlling steering angle after twiddle
	double Kp_a = 0.149506;
	double Ki_a = 0.000104455;
	double Kd_a = 3.59733;
	
	// Initialize the pid variable.
	pid_a.init(Kp_a, Ki_a, Kd_a);
	p_a = { Kp_a, Ki_a, Kd_a };

	// final set of PID parameters for controlling throttle after twiddle
	double Kp_t = 0.000137959; // 0.3;
	double Ki_t = 0.0;
	double Kd_t = 0.208181; // 0.02;
	pid_t.init(Kp_t, Ki_t, Kd_t);
	p_t = { Kp_t, Ki_t, Kd_t };

	// probing values for twiddle
	dp = { 0.1*Kp_a, 0.1*Ki_a, 0.1*Kd_a };

	double error;
	run_simulator(h, p_a, p_t, pid_a, pid_t, dp, error, resultsFile);
	
	// We don't need this since we're not using HTTP but if it's removed the program
	// doesn't compile :-(
	h.onHttpRequest([](uWS::HttpResponse *res, uWS::HttpRequest req, char *data, size_t, size_t)
	{
		const string s = "<h1>Hello world!</h1>";
		if (req.getUrl().valueLength == 1)
		{
			res->end(s.data(), s.length());
		}
		else
		{
			// i guess this should be done more gracefully?
			res->end(nullptr, 0);
		}
	});

	h.onConnection([&h, &is_connected](uWS::WebSocket<uWS::SERVER>* ws, uWS::HttpRequest req)
	{	
		is_connected = true;
		cout << "Connected!!!" << endl;
	});

	h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER>* ws, int code, char *message, size_t length)
	{
		ws->close();
		cout << "Disconnected" << endl;
	});

	int port = 4567;
	if (h.listen("127.0.0.1", port))
	{		
		cout << "Listening to port " << port << endl;
	}
	else
	{
		cerr << "Failed to listen to port" << endl;
		return -1;
	}
	h.run();
}

	  
