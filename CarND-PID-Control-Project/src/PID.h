#ifndef PID_H
#define PID_H

#include <vector>

using namespace std;

class PID 
{
public:
  /* Twiddle related */
	int nsteps;
	double cum_tot_error;

  /*
    Previous value of cte
  */
  double prev_cte;
  /*
  * Errors
  */
  double p_error;
  double i_error;
  double d_error;

  /*
  * Coefficients
  */ 
  double Kp;
  double Ki;
  double Kd;

  /*
  * Constructor
  */
  PID();

  /*
  * Destructor.
  */
  virtual ~PID();

  /*
  * Initialize PID.
  */
  void init(double Kp, double Ki, double Kd);

  /*
  * Update the PID error variables given cross track error.
  */
  void update_error(double cte);

  /*
  * Calculate the total PID error.
  */
  double total_error();

};

#endif /* PID_H */
