#include <iostream>
#include "PID.h"

using namespace std;

const int num_params = 3;

/*
* TODO: Complete the PID class.
*/

PID::PID() {}

PID::~PID() {}

// initialize PID controller member variables
void PID::init(double Kp_in, double Ki_in, double Kd_in)
{
	Kp = Kp_in;
	Ki = Ki_in;
	Kd = Kd_in;

	prev_cte = 0.76;
	
	p_error = 0;
	d_error = 0;
	i_error = 0;

	cum_tot_error = 0.0;
	nsteps = 0;
}

// update different errors of PID controller based on input cross trek error
void PID::update_error(double cte)
{
	nsteps += 1;

	p_error = cte;
	d_error = cte - prev_cte;
	i_error += cte;
	prev_cte = cte;

	return;
}

// calculate total error and update cumulative total error
double PID::total_error()
{
	double total_error = Kp * p_error + Ki * i_error + Kd * d_error;

	cum_tot_error += (total_error*total_error);

	return total_error; 
}



