
#  PID controller for driving car around race track 

We use controls to manage steering, brake, and throttle to actuate a car on its stipulated path. When we as humans turn through an intersection, we use our intuition and experience to determine how hard to steer, when to accelerate, and when to apply brakes. In vehicles, the control algorithms that manage the actuators are often called controllers. One of the most common and fundamental controllers is the proportional-integral-differental (PID) controller.

The goal of this project is to implement a PID controller in C++ to maneuver the vehicle around the simulator track.

##  PID control theory 

Consider the following car shown in Figure 1. We wish to drive the car smoothly along the reference trajectory. Let us assume that the car has a fixed forward velocity, and that you have the ability to set the steering angle of the car. Cross trek error (CTE) is the lateral distance between the vehicle and the reference trajectory. The larger the CTE, the more you turn towards the reference trajectory. On the other hand, keeping the steering angle constant will put you on a circle.

[image1]: ./figures/Problem.jpg "Control problem introduction"
![alt text][image1]
**Figure 1 Control problem introduction**

###  P-controller 

$\alpha$ = -$\tau_{P}$.CTE 

Steering angle, $\alpha$ is proportional to CTE and proportionality constant $\tau_{P}$ is a control parameter that governs the proportional response of the steering angle to CTE. No matter how small $\tau_{P}$ is, the steering angle will overshoot. In other words, the car will eventually turn its wheels quite a bit toward its trajectory, i.e., move towards the trajectory more and more, and when the car hits the trajectory, its wheels will turn straight, but the car itself will be oriented a bit downwards, and is forced to overshoot.

As illustrated in Figure 2, assuming the reference trajectory is x-axis, then y-axis value is the same as CTE. By turning inversely proportional to the y-value using parameter $\tau_{P}$ that sets the response strength of the proportional controller, the car should turn towards the x-axis, drive in that direction, overshoot, turn around, and drive back. As $\tau_{P}$ increases, the car wobbles more and convergence to a reference trajectory will take much longer.

[image2]: ./figures/P_controller.jpg "P-controller"
![alt text][image2]
**Figure 2 P-controller**

###  PD-controller 

Is there a way to avoid this overshoot? The trick is called PD control. In PD control, the steering angle $\alpha$ is not just related to CTE by virture of the proportional gain parameter $\tau_{P}$, but also to the temporal derivative, dCTE/dt. What this means is that when the car has turned enough to reduce the CTE, it won't just go shooting for the x-axis, but it will notice that it is already reducing the error, i.e., the error is becoming smaller over time, it will counter steer and then steer up again to gracefully approach the target trajectory, assuming appropriate settings of our differential gain $\tau_{D}$ in relation to the proportional gain $\tau_{P}$.

$\alpha$ = -$\tau_{P}.CTE - \tau_{D}.(CTE_{t} - CTE_{t-1})$, assuming $\Delta$t = t-(t-1) = 1

[image3]: ./figures/PD_controller.jpg "PD-controller"
![alt text][image3]
**Figure 3 PD-controller**

###  Systematic bias and PID-controller 

As illustrated in Figure 4, when there is a wheel alignment issue, i.e., the wheels are aligned with bit of an angle, car with even PD-controller will experience an error caused by so-called systematic bias. Even though the bias was in steering, it manifests itself as an increased CTE in the y-direction.

[image4]: ./figures/Systematic_bias.jpg "Systematic bias"
![alt text][image4]
**Figure 4 Systematic bias**

If you drive a car and your normal steering mode with PD-controller leads you to a trajectory far away from the goal and you notice over a long period of time that you cannot get closer to the reference trajectory. You start steering more and more towards the reference trajectory to compensate for the bias. To do so, you need a sustained situation of large CTE, which is measured by the integral or sum of CTE over time.

$\alpha$ = -$\tau_{P}.CTE - \tau_{D}.(CTE_{t} - CTE_{t-1}) - \tau_{I}.\sum CTE_{t}$

Larger the parameter $\tau_{I}$, faster is the convergence.

###  Twiddle algorithm for parameter optimization 

In order to find optimal set of parameters, a local hill climbing optimization algorithm called Twiddle is often used. It is also called coordinate ascent algorithm. Figure 5 depicts the Twiddle algorithm implemented in Python.

In the beginning, a probing interval corresponding to each parameter is initialized to some appropriate value. For each parameter in isolation, the algorithm moves the parameter by probing the parameter value up or down a little bit by adding or subtracting the probing interval from the parameter. If it finds a better solution, it retains it and even increases the probing interval. If it fails to find a better solution, it goes back to the original and even decreases the probing interval. This is done so long as the sum of probing intervals for all the parameters is larger than the tolerance threshold. The goodness of solution is measured by appropriate cost function such as average CTE, for example.

[image5]: ./figures/Twiddle.jpg "Twiddle algorithm"
![alt text][image5]
**Figure 5 Twiddle algorithm**

##  Project details 

For this project, we have implemented two separate PID controllers, one for controlling the steering angle, and the other for controlling the throttle. The parameters were initially picked manually, and fine tuned using the twiddle algorithm. This was done separately for the steering angle and the throttle.

###  Steering angle 

The steering angle PID controller is tuned to the actual value of the CTE as the direction of CTE impacts the steering angle. First, we worked on optimizing the PID controller parameters for steering angle by setting the throttle to a fixed value of 0.3. The initial starting manual values were based on the class notes $\tau_{P}$ = 0.2, $\tau_{I}$ = 0.004, and $\tau_{D}$ = 3.0. The car wobbled a bit, but was able to complete rounds on the simulator track. We then manually set the values to $\tau_{P}$ = 0.1, $\tau_{I}$ = 0.0001, and $\tau_{D}$ = 3.0, and the behavior was much more stable. 

We further refined the parameter values with the twiddle algorithm by initializing the probing intervals to 0.1 times each parameter, tolerance value to 0.00001, and cost function to be the average error. The parameters were "twiddled" until the sum of the probing intervals was less than the tolerance value. Each iteration consisted of driving around the track with the updated set of parameter values. The drive around the track at the throttle value of 0.3 consisted of ~2900 steps. On every iteration, the car was allowed to settle for first 100 steps during which the error was not accumulated; then the error was accumulated for 2800 steps, after which the cumulative error was compared with the previous best error. This comparison was used to direct the probing of parameter values with the probing intervals, and also adjust the probing intervals.

total error, TE = $\tau_{P}$.CTE + $\tau_{D}.(CTE_{t} - CTE_{t-1}$) + $\tau_{I}.\sum CTE_{t}$.

average error = cumulative running average of $TE^{2}$

The final values of parameters were: $\tau_{P}$ = 0.149506, $\tau_{I}$ = 0.000104455, and $\tau_{D}$ = 3.59733.

To gain better intuition regarding the impact of each parameter on the simulator, we tried with different combinations of parameters such as ID, PI, PD, and PID at throttle of 0.3. We increased the throttle to 0.6, and found that it did not impact the performance much. As shown in Figure 6, the car falls off the track without the P component after few steps, runs all over the place without the D component, whereas the impact of I component is subtle. With the PD components only, the car is able to drive around the track, but with the I component added, it wobbles a bit initially, but once stabilized, the corner handling improves a bit.

[image6]: ./figures/Impact_of_different_components.jpg "Impact of different components"
![alt text][image6]
**Figure 6 Impact of different components**

Figure 7 depicts the impact of different values of $\tau_{D}$, keeping $\tau_{P}$ and $\tau_{I}$ fixed. With $\tau_{D}$ = 100, the car wobbles a lot and then runs off the track; with $\tau_{D}$ = 5, the car is able to complete the track, but is not able to handle the corner well; with $\tau_{D}$ = 1, the car wobbles a lot, and not able to complete the track; with $\tau_{D}$ = 3, the behavior is much more stable.

[image7]: ./figures/Impact_of_different_values_of_Kd.jpg "Impact of different values of $\tau_{D}$"
![alt text][image7]
**Figure 7 Impact of different values of $\tau_{D}$**

##### Steering angle = -total error #####

###  Throttle 

We then proceeded to tune the PID controller parameters for throttle. The throttle PID controller is tuned to the absolute value of the CTE, unlike the steering angle controller which is tuned to the actual value of CTE as the direction of CTE impacts the steering angle. For this reason, the throttle controller does not include an I component, which would only grow indefinitely. The initial values were manually set to $\tau_{P}$ = 0.3, $\tau_{I}$ = 0.0, and $\tau_{D}$ = 0.02. After twiddle (same as for steering angle), the final values converged to $\tau_{P}$ = 0.000137959, $\tau_{I}$ = 0.0, and $\tau_{D}$ = 0.208181.

##### Throttle = th - total error #####

With the above set of parameters, the car could throttle upto $th$ = 0.6, which is equivalent to 65 mph. Figure 8 depicts the behavior with $th$ values of 0.55, 0.6, 0.65, 0.7, and 0.75. For values higher than 0.6, the car was crossing the red line around sharp corners, especially the one after the lake.

[image8]: ./figures/Impact_of_different_values_of_th.jpg "Impact of different values of th"
![alt text][image8]
**Figure 8 Impact of different values of $th$**




<video controls src="videos/PID_controller_demo.mp4" />

##  File structure 

The generalized overall flow of PID control algorithm consists of 
1. creating PID controller objects for steering angle and throttle 
2. initializing them with the parameter values once 
3. passing value of CTE from simulator to the PID objects 
4. collecting total error, one from each PID objects 
5. calculating steering angle value and throttle value based on respective total errors 
6. repeat steps 3 to 5. 

Following are the project files in the *src* folder:

* *main.cpp* - 
  * main() - 
    - creates PID controller objects for steering angle and throttle
    - initializes them with the parameter values once 
    - passes value of CTE from simulator to the PID objects 
    - collects total error, one from each PID object
    - calculates steering angle value and throttle value based on respective total errors
  * run_simulator() - 
    - calls the simulator and passes in the PID parameters for steering angle and throttle to the simulator
    - runs twiddle if ```twiddle = true```
  * run_simulator_internal() - runs one step of the simulator
  * reset_simulator() - resets the simulator and places the car at the starting position
  * reset_and_init() - calls reset_simulator() and initializes the PID controllers with the updated values of the parameters
  * dp_sum() - calculates the sum of the probing values for all the PID parameters for twiddle algorithm
* *PID.cpp* - 
  * init() - initializes PID controller member variables
  * update_error() - updates different errors of PID controller based on input CTE
  * total_error() - calculates total error and updates cumulative total error.

The Term 2 simulator is a client, and the C++ program software is a web server. *main.cpp* reads in the CTE value from the client and passes to the PID controller object for processing. *main.cpp* is made up of several functions within main(), these all handle the uWebsocketIO communication between the simulator and it's self.

### Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the blog post that helped me setup the Visual studio environment for this project - http://www.codza.com/blog/udacity-uws-in-visualstudio. I would like to offer thanks to Jeremy Shannon as my approach to using twiddle algorithm for tuning parameters and using PID controller to tune throttle values were inspired by his work.
