# PID controller for driving car around race track

We use controls to manage steering, brake, and throttle to actuate a car on its stipulated path. When we as humans turn through an intersection, we use our intuition and experience to determine how hard to steer, when to accelerate, and when to apply brakes. Control algorithms are often called controllers. One of the most common and fundamental controllers is the proportional-integral-differental (PID) controller.

The goal of this project is to implement a PID controller in C++ to maneuver the vehicle around the simulator track. 

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-PID-Control-Project).

## Code base

The project was implemented in Visual Studio in Windows 10.

1. In the CarND-PID-Control-Project, open the pid_controller.sln file.
2. Rebuild Solution.
3. Open the simulator.
4. Click F5 in Visual Studio to debug. This attaches the code to the simulator.
5. Click "Start" on the simulator.

To compile the code in Linux or Mac OS or Docker environment, 
1. Go to CarND-PID-Control-Project\src folder
1. Delete main.cpp 
2. Rename main_other_env.cpp as main.cpp.

## Detailed writeup

Detailed report can be found in [_PID_control_writeup.md_](PID_control_writeup.md).

## Solution video

![](./videos/PID_controller_demo.mp4)

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the blog post that helped me setup the Visual studio environment for this project - http://www.codza.com/blog/udacity-uws-in-visualstudio. I would like to offer thanks to Jeremy Shannon as my approach to using twiddle algorithm for tuning parameters and using PID controller to tune throttle values were inspired by his work. 

